const express = require("express");
const http = require("http");
const path = require("path");
const socketio = require("socket.io");

// fire up express to get the express object
const app = express();
const server = http.createServer(app);
const io = socketio(server);

const port = process.env.PORT || 1337;
const publicDirectoryPath = path.join(__dirname, "../public");

// Middleware
app.use(express.static(publicDirectoryPath));

io.on("connection", (socket) => {
  socket.emit("message", "Welcome!");
  socket.broadcast.emit("message", "A new user has joined");
  socket.on("sendMessage", (message) => {
    io.emit("message", message);
  });

  socket.on("sendLocation", (coords) => {
    io.emit(
      "message",
      `Location https://google.com/maps?q=${coords.latitude},${coords.longitude}`
    );
  });

  socket.on("disconnect", () => {
    io.emit("message", "A user has left");
  });
});

// Launch the app server
server.listen(port, () => {
  console.log("Connected successfully");
});
